#  DOCBatch API
#  Copyright (c) 2021 Owen McGinley

#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.

import boto3
import requests
from datetime import datetime, timedelta
from pytz import timezone
import configparser

configfile = "config.cfg"
config = configparser.ConfigParser
config.read(configfile)

access_key_aws = config['AWS']['access_key']
secret_key_aws = config['AWS']['secret_key']
region_aws = config['AWS']['region']
table_name_aws = config['AWS']['dbname']

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table(table_name_aws)

siteid = config['DOC']['siteid']
doc_timezone = config['DOC']['timezone']
timezone_name = config['DOC']['timezone_name']

doc_data = requests.get("https://api.dineoncampus.com/v1/locations/mobile_locations.json?site_id=%s" % siteid)
doc_data = doc_data.json()
purenow = datetime.now()
now = datetime.now()
parsed_date = now.strftime("%Y-%m-%d")
hours_data = requests.get("https://api.dineoncampus.com/v1/locations/weekly_schedule?site_id=%s&date=%s%s" % (siteid, parsed_date, doc_timezone))
hours_data = hours_data.json()
weekout = now + timedelta(days=7)
parsed_date = weekout.strftime("%Y-%m-%d")
hours_data_secondary = requests.get("https://api.dineoncampus.com/v1/locations/weekly_schedule?site_id=%s&date=%s%s" % (siteid, parsed_date, doc_timezone))
hours_data_secondary = hours_data_secondary.json()

location_array = doc_data['locations']
hours_array = hours_data['the_locations']
hours_secondary_array = hours_data_secondary['the_locations']
known_location_ids = []

scan = table.scan()
with table.batch_writer() as batch:
    for each in scan['Items']:
        batch.delete_item(
            Key={
                'uuid': each['uuid'],
            }
        )

for i in range(0, len(location_array)):
    table.put_item(
        Item={
            'uuid': location_array[i]['id'],
            'name': location_array[i]['name'],
            'lat': repr(location_array[i]['address']['lat']),
            'lng': repr(location_array[i]['address']['lon']),
            'reservations': location_array[i]['use_reservations']
        }
    )
    known_location_ids.append(location_array[i]['id'])


tz = timezone(timezone_name)

for location in hours_array:
    if location['id'] in known_location_ids:
        location_hours_array = []
        for day in location['week']:
            dayofweek = datetime.strptime(day['date'], "%Y-%m-%d")
            for interval in day['hours']:
                starttime = tz.localize(datetime.strptime(day['date'], "%Y-%m-%d").replace(hour=interval['start_hour'],
                                                                               minute=interval['start_minutes']))
                endtime = tz.localize(datetime.strptime(day['date'], "%Y-%m-%d").replace(hour=interval['end_hour'],
                                                                             minute=interval['end_minutes']))
                location_hours_array.append(int(starttime.timestamp()))
                location_hours_array.append(int(endtime.timestamp()))
        table.update_item(
            Key={
                'uuid': location['id']
            },
            AttributeUpdates={
                'thisweek_hours': {
                    "Value": {
                        "L": location_hours_array
                    },
                    "Action": "PUT"
                }
            }
        )

for location in hours_secondary_array:
    if location['id'] in known_location_ids:
        location_hours_array = []
        for day in location['week']:
            dayofweek = datetime.strptime(day['date'], "%Y-%m-%d")
            for interval in day['hours']:
                starttime = tz.localize(datetime.strptime(day['date'], "%Y-%m-%d").replace(hour=interval['start_hour'],
                                                                               minute=interval['start_minutes']))
                endtime = tz.localize(datetime.strptime(day['date'], "%Y-%m-%d").replace(hour=interval['end_hour'],
                                                                             minute=interval['end_minutes']))
                location_hours_array.append(int(starttime.timestamp()))
                location_hours_array.append(int(endtime.timestamp()))
        table.update_item(
            Key={
                'uuid': location['id']
            },
            AttributeUpdates={
                'nextweek_hours': {
                    "Value": {
                        "L": location_hours_array
                    },
                    "Action": "PUT"
                }
            }
        )

print("Script complete.")


