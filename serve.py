#  DOCBatch API
#  Copyright (c) 2021 Owen McGinley

#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.

from flask import Flask, request, jsonify, render_template
import boto3
import time
from datetime import datetime, timedelta
import requests
import configparser

configfile = "config.cfg"
config = configparser.ConfigParser()
config.read(configfile)

access_key_aws = config['AWS']['access_key']
secret_key_aws = config['AWS']['secret_key']
region_aws = config['AWS']['region']
table_name_aws = config['AWS']['dbname']

session = boto3.Session(aws_access_key_id=access_key_aws, aws_secret_access_key=secret_key_aws, region_name=region_aws)
db = session.resource('dynamodb')
table = db.Table(table_name_aws)

api_version = "v2.0.2"

app = Flask(__name__)


@app.route("/", methods=['GET'])
def main():
    return "This is the supplemental API for DOCBatch. Acts as a intermediate cache for the Dine On Campus Locations " \
           "API to speed up loading times. <br><br>Call by using api.docbatch.app/api/v1/locations. No key required. <br><br>Updated at " \
           "3 AM, 11 AM, 7 PM EST daily. Please be reasonable with request counts. Thanks! "


@app.route("/api/v1/reservefornow", methods=['POST'])
def rfn():

    cust_id = request.values.get("userid")
    loc_id = request.values.get("locid")

    if cust_id is None or cust_id == "" or loc_id is None or loc_id == "":
        response = {"apiversion": api_version, "code": 2, "usererror": True, "message": "No customer ID or location ID entered."}
        response = jsonify(response)
        response.headers.add('Access-Control-Allow-Origin', "*")
        return response

    currenttime = datetime.fromtimestamp(time.time())
    currenthr = currenttime.hour
    currentmin = currenttime.minute
    if currentmin <= 14:
        currentmin = 0
    elif currentmin <= 44:
        currentmin = 30
    else:
        currenttime = currenttime + timedelta(hours=1)
        currenthr = currenttime.hour
        currentmin = 0

    currentdate = currenttime.strftime("%Y-%m-%d")

    requestdata = {
        "customer_id": cust_id,
        "date": currentdate,
        "friends": 0,
        "hour": currenthr,
        "minute": currentmin,
        "location_id": loc_id,
    }

    try:
        docrequest = requests.post("https://api.dineoncampus.com/v1/reservations", data=requestdata)
    except:
        response = {"apiversion": api_version, "code": 3, "docerror": True, "message": "Failed to make reservation through Dine On Campus."}
        response = jsonify(response)
        response.headers.add('Access-Control-Allow-Origin', "*")
        return response

    response = {"apiversion": api_version, "code": 0, "noerror": True, "data": docrequest.text, "message": "Reservation made."}
    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', "*")
    return response


@app.route("/api/v1/locations", methods=['GET'])
def locations():
    aws_response = table.scan()
    response_data = {}
    for i in aws_response['Items']:
        id = str(i['uuid'])
        name = str(i['name'])
        lat = float(i['lat'])
        lng = float(i['lng'])
        reservations = i['reservations']
        thisweekhours = i['thisweek_hours']
        nextweekhours = i['nextweek_hours']
        response_data[id] = {
            "name": name,
            "lat": lat,
            "lng": lng,
            "reservations": reservations,
            "thisweekhours": thisweekhours,
            "nextweekhours": nextweekhours
        }
    response = {"apiversion": api_version, "data": response_data, "code": 0, "message": "Success"}
    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', "*")
    return response


@app.route("/api/v1/alwaysfail", methods=['POST'])
def alwaysfail():
    #response = {"status": "error", "msg": "The time slot you requested is no longer available."}
    response = {"status": "success", "msg": "it worked dumbo"}
    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', "*")
    return response
