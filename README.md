# DOCBatch API
Intermediate cache based on DynamoDB to speed up requests to the Dine On Campus Locations API (and do reserve for now requests from Siri Shortcuts). It's essential for the functioning of DOCBatch 2 & 3.

# Looking for DOCBatch 2/3?
DOCBatch 2 is available here: https://gitlab.com/o355/docbatch-2

DOCBatch 3 is available here: https://gitlab.com/o355/docbatch-3

# Spaghet-o-meter
Projects made before late 2020 generally focused more on development speed, rather than reusable code. Because the DOCBatch API was made before I began to learn about reusable code, there's definitly a lot of spaghetti code everywhere.

A rating of 7/10 is suitable for this project.

# Setup
(The DOCBatch API was made very hastily, and requires a specific setup. I may refactor the DOCBatch API if dining reservations continue to be used in the 2021-22 school year (at that point DOCBatch 4 would be worked on - but I'm waiting for an announcement about that). Time will only well. Regardless - I tried to make these setup docs as descriptive as possible.)

The DOCBatch API is a Flask-based API that runs entirely in Python. To run this, you'll need a decent chunk of libraries, which you can install via pip3. These libraries are:
* boto3
* requests
* datetime
* pytz
* flask

You can install them with a one-liner command below:

`pip3 install boto3 requests datetime pytz flask`

The DOCBatch API relies on a DynamoDB database for fast access. I know what you're saying,

> Wouldn't you want to use a local JSON file so it's faster?

Yes, yes indeed. DOCBatch 1.0 was made in about 48 hours, and this was what I was most comfortable with at the time.

Regardless, you'll want to get an AWS account set up with DynamoDB. The DOCBatch API should be able to run within the free tier limits with no worries. Once you can set up a database, set a database with this configuration:
* Name - Whatever you want it to be, just keep note of it.
* Primary key - `uuid` as String.
* Secondary key - none
* Any other options - leave them blank
* Read/Write Capacity Units - 1 is fine, go higher if you need to. You get 25 read & 25 write units on the free tier, so you have some room to grow.

You'll then want to visit the IAM section of AWS, and generate access/secret keys that have full access to your new DynamoDB database. Keep note of the access/secret keys.

You'll now need to get the region code for wherever your DynamoDB is being hosted. Your region name is in the top right of the AWS console - and the region code is **usually** the first part of the domain for the console `us-east-2.extrashithere`, where `us-east-2` is your region code. You can also look up the region code that corresponds with your region name.

### Putting all your stuff in config.cfg at this point
config.cfg has an AWS section where you can put your access key, secret key, DB name, and region name. Put that in there at this point.

### Getting the Dine On Campus Site ID
Now it's time to determine the site ID for where you want to use the DOCBatch API. The best way to do this is go to Dine On Campus' website for your school. Make sure you open inspect element, and switch to the Network tab. You'll see some requests come in to `api.dineoncampus.com`, with a Site ID attached to the request as a query parameter.

Put that site ID in config.cfg under `DOC` in the field `siteid`.

### Dancing with the weirdness of DOC Timezones
The way Dine On Campus does timezones are VERY weird - and I kinda forget it. There's two fields under the `DOC` section of config.cfg - the timezone field, and then the timezonename field.

The timezone field is `T04:00:00.000Z` by default, with `timezonename` as America/New_York. If you're running DOCBatch in a different timezone, you'll want to change these accordingly. Make sure your `timezone` field is **POSITIVE**. Remember when I said Dine On Campus does times weirdly?

For instance - Pacific Time should be done with timezone as `T07:00:00.000Z` and `timezonename` as America/Los_Angeles. You may need to experiment with this to make sure things are working properly.

### Getting things going
Finally, `getlocations.py` should run and get the location data. If there's any errors - it's possible you misconfigured things. At this point, you'll want to get this script to run every 6-12 hours or so, cron works fine for this.

### Setting your absolute config.cfg filepath
In both serve.py and getlocations.py, you'll want to modify the line that has `configfile = ` so that the config.cfg file is pointed at an absolute path (rather than relative). This will save you a lot of headaches when you start running `getlocations.py` with cron, and eventually host `serve.py` with WSGI.

### Serving the API
Serving with a WSGI server (either Apache/Nginx) works. Make sure you run it in daemon mode for fast loading times. Lastly, modify your copy of DOCBatch so it points to your new DOCBatch API.

# A very rough description of the API methods
Two api methods. /api/v1/reservefornow, and /api/v1/locations.

## /api/v1/locations (GET)
The main thing to know is how responses are structured. The data you're looking for is under the data key. You'll need to iterate `data` by key name, each key representing the UUID of the location, with a dictionary for specific information.

Inside each location dictionary, there's important fields for DOCBatch's use. The latitude & longitude of the location (`lat` and `lng` respectively), whether the location takes reservations `reservations`, and the `name`.

The hours are the very fun part of this adventure. There's `thisweekhours` and `nextweekhours`. In each section, you'll find an array of UNIX timestamps. Even-numbered indices represent when a location opens, and odd-numbered indices represent when a location closes. Between those two timestamps is when the location is open.

For example, indice 0 represents the closest opening time of a location (the start of an "interval"), while indice 1 represents the closest closing time of a location (the end of an "interval"). The time in between the start/end of the "interval" is when the location is open.

Indice 2 then represents the next closest opening time of a location (the start of another "interval"), and indice 3 represents the next closest closing time of a location (the end of that "interval" we started on indice 2). The next timeframe for being open is the time between the UNIX stamp at indice 2 & 3.

This definitely isn't the best system to organize this - but this was specific method was made back in September 2020 when I knew MUCH less about software development. If I refactor the API, this method is getting updated (with a new /api/v2/locations endpoint to retain backwards compatibility).

## /api/v1/reservefornow (POST)
This method is exclusively for sending up a POST request to make a reservation for now at a specific location. This API method skips the closing checks that the main DOCBatch program does (along with doing the whole don't make a reservation for the closing time logic), so it's pretty rough.

Send up your DOC User ID in the `userid` query parameter, and the location UUID for wherever you want to reserve for in the `locid` query parameter. Don't worry - the server only keeps your user ID in memory, and is never actually stored.

Depending on your logging setup, you'll probably want to send this data as form data (rather than query parameters). Some weblogs may log the query parameters attached to a request, thus storing user IDs.

There's a variety of responses if something goes wrong - check the source code for more info.

# Licensing
The DOCBatch API is under the MIT License. 
